# cern-sssd-conf

* This is a simple rpm to package and distribute `/etc/sssd/conf.d/00_cern.conf`
* sssd is has an implict "includedir" of `/etc/sssd/conf.d/` in CC7 and C8, even if a `/etc/sssd/sssd.conf` file does not exist
* As this package does not overwrite `/etc/sssd/sssd.conf`, it's safe to be used on all hosts

* `/etc/sssd/conf.d/00_cern.ch` provides a minimum sssd configuration that allows pam_sss to function correctly at CERN
    * this configuration does not provide ldap integration, or filtering
    * extra functionality via subpackages that deploy additional files in `/etc/sssd/conf.d` may be released at a future date 
