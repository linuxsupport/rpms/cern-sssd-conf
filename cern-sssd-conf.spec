Name:          cern-sssd-conf
Version:       1.5
Release:       2%{?dist}
Summary:       A simple sssd configuration file to be used at CERN
Group:         CERN/Utilities
License:       BSD
URL:           http://linux.cern.ch
Source0:       %{name}-%{version}.tgz
BuildArch:     noarch
Requires:      sssd, sssd-krb5
%if 0%{?el7}%{?el8}
BuildRequires: systemd
%else
BuildRequires: systemd-rpm-macros
%endif
Conflicts:     cern-sssd-conf-cernch, cern-sssd-conf-global, cern-sssd-conf-global-cernch, cern-sssd-conf-domain-cernch, cern-sssd-conf-servers-cernch-gpn

%description
This rpm provides a basic sssd configuration file to be used at CERN

%package cernch
Requires:      cern-sssd-conf-global-cernch = %{version}-%{release}
Requires:      cern-sssd-conf-domain-cernch = %{version}-%{release}
Summary:       SSSD configuration for CERN's main domain
%description cernch
SSSD configuration to use CERN's main identity management infrastructure

%package global
Requires:      sssd
Summary:       SSSD global configuration fragment for CERN domains
%description global
SSSD global configuration fragment to use CERN's identity management
infrastructure

%package global-cernch
Requires:      sssd
Requires:      cern-sssd-conf-global = %{version}-%{release}
Summary:       SSSD global configuration fragment for CERN's main domain
%description global-cernch
SSSD global configuration fragment to use CERN's main identity management
infrastructure

%package domain-cernch
Requires:      sssd, CERN-CA-certs, sssd-ldap, sssd-krb5
Requires:      cern-sssd-conf-servers-cernch-gpn = %{version}-%{release}
Summary:       SSSD configuration fragment for CERN's main domain
%description domain-cernch
SSSD configuration fragment to use CERN's main identity management
infrastructure

%package servers-cernch-gpn
Requires:      sssd, CERN-CA-certs, sssd-ldap, sssd-krb5
Summary:       SSSD configuration fragment for the servers of CERN's main domain
%description servers-cernch-gpn
SSSD configuration fragment for the servers of CERN's main domain

%prep
%setup -q

%build

%install
install -d %{buildroot}/%{_sysconfdir}/sssd/conf.d
%if 0%{rhel} <= 7
install -p -m 0600 sssd.conf %{buildroot}/%{_sysconfdir}/sssd/sssd.conf
%endif
install -p -m 0600 00_cern.conf %{buildroot}/%{_sysconfdir}/sssd/conf.d/00_cern.conf
install -p -m 0600 00_cern-global.conf %{buildroot}/%{_sysconfdir}/sssd/conf.d/00_cern-global.conf
install -p -m 0600 01_cern-global-cernch.conf %{buildroot}/%{_sysconfdir}/sssd/conf.d/01_cern-global-cernch.conf
install -p -m 0600 02_cern-domain-cernch.conf %{buildroot}/%{_sysconfdir}/sssd/conf.d/02_cern-domain-cernch.conf
install -p -m 0600 03_cern-servers-cernch-gpn.conf %{buildroot}/%{_sysconfdir}/sssd/conf.d/03_cern-servers-cernch-gpn.conf

%files
%config %{_sysconfdir}/sssd/conf.d/00_cern.conf

%files global
%if 0%{rhel} <= 7
%config(noreplace) %{_sysconfdir}/sssd/sssd.conf
%endif
%config %{_sysconfdir}/sssd/conf.d/00_cern-global.conf

%files global-cernch
%config %{_sysconfdir}/sssd/conf.d/01_cern-global-cernch.conf

%files domain-cernch
%config %{_sysconfdir}/sssd/conf.d/02_cern-domain-cernch.conf

%files servers-cernch-gpn
%config %{_sysconfdir}/sssd/conf.d/03_cern-servers-cernch-gpn.conf

%post
if [ $1 -eq 1 ] ; then
  # Initial installation
  systemctl try-restart sssd.service &>/dev/null || :
fi

%post global
if [ $1 -eq 1 ] ; then
  # Initial installation
  systemctl try-restart sssd.service &>/dev/null || :
fi

%post domain-cernch
if [ $1 -eq 1 ] ; then
  # Initial installation
  systemctl try-restart sssd.service &>/dev/null || :
fi

%post servers-cernch-gpn
if [ $1 -eq 1 ] ; then
  # Initial installation
  systemctl try-restart sssd.service &>/dev/null || :
fi


%preun
if [ $1 -eq 0 ] ; then
  # Package removal
  systemctl try-restart sssd.service &>/dev/null || :
fi

%preun global
if [ $1 -eq 0 ] ; then
  # Package removal
  systemctl try-restart sssd.service &>/dev/null || :
fi

%preun domain-cernch
if [ $1 -eq 0 ] ; then
  # Package removal
  systemctl try-restart sssd.service &>/dev/null || :
fi

%preun servers-cernch-gpn
if [ $1 -eq 0 ] ; then
  # Package removal
  systemctl try-restart sssd.service &>/dev/null || :
fi

%postun
%systemd_postun_with_restart sssd.service # Package upgrade

%postun global
%systemd_postun_with_restart sssd.service # Package upgrade

%postun domain-cernch
%systemd_postun_with_restart sssd.service # Package upgrade

%postun servers-cernch-gpn
%systemd_postun_with_restart sssd.service # Package upgrade

%changelog
* Thu Mar 2 2023 Steve Traylen <steve.traylen@cern.ch> 1.5-2
- Scriptlets will only call try-restart sssd.service now.

* Fri Nov 11 2022 Steve Traylen <steve.traylen@cern.ch> 1.5-1
- Introduce xldap-critical as backup ldap_uri
- Use systemd-rpm-macros as BR on 9 and newer

* Thu Mar 03 2022 Ben Morrice <ben.morrice@cern.ch> 1.4-1
- remove deprecated ldap_initgroups_use_matching_rule_in_chain config

* Fri Jan 14 2022 Ben Morrice <ben.morrice@cern.ch> 1.3-1
- remove ipa configuration

* Mon May 17 2021 Julien Rische <julien.rische@cern.ch> 1.2-2
- Use unqualified username format for default domain only

* Tue Apr 27 2021 Julien Rische <julien.rische@cern.ch> 1.2-1
- Support user information lookup for main and IPAdev CERN domains

* Wed Jun 24 2020 Julien Rische <julien.rische@cern.ch> 1.1-1
- Add subpackage for IPAdev configuration

* Tue Apr 21 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-2
- remove Requires: sssd-kcm

* Fri Jan 17 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
